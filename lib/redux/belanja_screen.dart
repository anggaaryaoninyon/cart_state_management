import 'package:flutter/material.dart';

import '../barang.dart';

class BelanjaScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Container(),
        title: Text('Swalayan Barang'),
        centerTitle: true,
      ),
      body: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          ListView(
            children: generateBarangList()
                .map((e) => ListTile(
              title: Text(e.nama),
              leading: Container(
                width: 64,
                height: 64,
                color: e.color,
              ),
              subtitle: Text('Rp ${e.harga}'),
              onTap: () {

                // TODO ketika barang di pencet, masuk ke keranjang



              },
            ))
                .toList(),
          ),
          cartWidget()
        ],
      ),
    );
  }

  Widget cartWidget() {
    return AnimatedSwitcher(
        duration: Duration(milliseconds: 750),
        transitionBuilder: (Widget child, Animation<double> animation) {
          return ScaleTransition(child: child, scale: animation);
        },
        child: true // TODO Check Cart empty atau tidak
            ? cartEmptyWidget()
            : cartNotEmptyWidget()
    );
  }

  Widget cartNotEmptyWidget(){

  }

  Container cartCloseWidget() {
    return Container(
      child: Row(
        children: [
          Expanded(
            child: Container(
              height: 0,
            ),
          ),
          Container(
            padding: EdgeInsets.all(4),
            color: Colors.black54,
            child: IconButton(
                icon: Icon(
                  Icons.shopping_cart,
                  color: Colors.white,
                ),
                onPressed: () {
                  // TODO buka cart
                }),
          )
        ],
      ),
    );
  }

  Container cartOpenWidget() {
    return Container(
      width: double.infinity,
      color: Colors.black54,
      padding: EdgeInsets.symmetric(vertical: 14),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 14),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment:
              MainAxisAlignment.spaceBetween,
              children: [
                Text('Rp ==TOTAL==',
                    style: TextStyle(
                        fontSize: 24,
                        color: Colors.white,
                        fontWeight: FontWeight.bold)),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    IconButton(
                        icon: Icon(Icons.clear_all,
                            color: Colors.white),
                        onPressed: () {
                          // TODO clear cart
                        }),
                    IconButton(
                        icon: Icon(Icons.send,
                            color: Colors.white),
                        onPressed: () {
                          // TODO Goto Cart Screen
                        }),
                    IconButton(
                        icon: Icon(Icons.expand_more,
                            color: Colors.white),
                        onPressed: () {
                          // TODO tutup cart
                        }),
                  ],
                )
              ],
            ),
          ),
          Container(
              height: 78,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: [].map((element) => Container(
                  width: 64,
                  height: 64,
                  color: element.color,
                  margin: EdgeInsets.all(7),
                )).toList(),
              ))
        ],
      ),
    );
  }

  Container cartEmptyWidget() => Container();
}
