import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class CartScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Column(
            children: [
              SizedBox(
                height: 32,
              ),
              Text('Rp ==TOTAL==',
                  style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold)),
              SizedBox(
                height: 32,
              ),
              Flexible(
                  child: ListView(
                    // TODO dari cart, tampilin barang yang dikeranjang
                    children: [].map((element) => ListTile(
                      title: Text(element.nama),
                      leading: Container(
                        width: 64,
                        height: 64,
                        color: element.color,
                      ),
                    )).toList(),
                  ))
            ],
          )),
    );
  }
}
