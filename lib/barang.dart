import 'dart:ui';

import 'package:flutter/material.dart';

class Barang{
  final Color color;
  final String nama;
  final int harga;

  Barang(this.color, this.nama, this.harga);
}

List<Barang> generateBarangList() => [
  Barang(Colors.red, 'Apel', 14000),
  Barang(Colors.green, 'Semangka', 23000),
  Barang(Colors.deepPurple, 'Terong', 7000),
  Barang(Colors.cyan, 'Es Krim', 3000),
  Barang(Colors.red, 'Apel A', 14000),
  Barang(Colors.green, 'Semangka A', 23000),
  Barang(Colors.deepPurple, 'Terong A', 7000),
  Barang(Colors.cyan, 'Es Krim A', 3000),
  Barang(Colors.red, 'Apel AA', 14000),
  Barang(Colors.green, 'Semangka AA', 23000),
  Barang(Colors.deepPurple, 'Terong AA', 7000),
  Barang(Colors.cyan, 'Es Krim AA', 3000),
  Barang(Colors.red, 'Apel B', 14000),
  Barang(Colors.green, 'Semangka B', 23000),
  Barang(Colors.deepPurple, 'Terong B', 7000),
  Barang(Colors.cyan, 'Es Krim B', 3000),
  Barang(Colors.red, 'Apel BB', 14000),
  Barang(Colors.green, 'Semangka BB', 23000),
  Barang(Colors.deepPurple, 'Terong BB', 7000),
  Barang(Colors.cyan, 'Es Krim BB', 3000),
  Barang(Colors.red, 'Apel C', 14000),
  Barang(Colors.green, 'Semangka C', 23000),
  Barang(Colors.deepPurple, 'Terong C', 7000),
  Barang(Colors.cyan, 'Es Krim C', 3000),
];