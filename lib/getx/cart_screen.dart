import 'package:cart_state_management/getx/cart_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';


class CartScreen extends StatelessWidget {
  CartController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Obx(
              () => Column(
                children: [
                  SizedBox(
                    height: 32,
                  ),
                  Text('Rp ${controller.totalPrice}',
                      style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold)),
                  SizedBox(
                    height: 32,
                  ),
                  Flexible(
                      child: ListView(
                        // TODO dari cart, tampilin barang yang dikeranjang
                        children: controller.cartList.map((element) => ListTile(
                          title: Text(element.nama),
                          leading: Container(
                            width: 64,
                            height: 64,
                            color: element.color,
                          ),
                        )).toList(),
                      ))
                ],
              )
          )),
    );
  }
}
