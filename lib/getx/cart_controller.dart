import 'package:get/get.dart';

import '../barang.dart';

class CartController extends GetxController{
  RxList<Barang> cartList = <Barang>[].obs;     // Untuk Menyimpan Barang di cart
  RxBool isCartOpened = false.obs;      // Untuk mengecek apakah widget cart sedang dibuka atau tidak
  RxInt totalPrice = 0.obs;

  void addBarang(Barang item){
    cartList.add(item);
    totalPrice.value += item.harga;
  }

  void clearCart(){
    cartList.clear();
    totalPrice.value = 0;
  }

  switchCartWidget(){
    isCartOpened.value = !isCartOpened.value;
  }
}