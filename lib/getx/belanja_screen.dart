import 'package:cart_state_management/getx/cart_screen.dart';
import 'package:cart_state_management/getx/cart_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../barang.dart';

class BelanjaScreen extends StatelessWidget {
  CartController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Container(),
        title: Text('Swalayan Barang'),
        centerTitle: true,
      ),
      body: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          ListView(
            children: generateBarangList()
                .map((item) => ListTile(
              title: Text(item.nama),
              leading: Container(
                width: 64,
                height: 64,
                color: item.color,
              ),
              subtitle: Text('Rp ${item.harga}'),
              onTap: () {

                // TODO ketika barang di pencet, masuk ke keranjang
                controller.addBarang(item);

              },
            )).toList(),
          ),
          cartWidget(context)
        ],
      ),
    );
  }

  Widget cartWidget(BuildContext context) {
    return AnimatedSwitcher(
        duration: Duration(milliseconds: 750),
        transitionBuilder: (Widget child, Animation<double> animation) {
          return ScaleTransition(child: child, scale: animation);
        },
        child: Obx(
            () => controller.cartList.isEmpty
                ? cartEmptyWidget()
                : cartNotEmptyWidget(context)
        )
    );
  }

  Widget cartNotEmptyWidget(BuildContext context){
    return Obx(
        () => controller.isCartOpened.value
            ? cartOpenWidget(context)
            : cartCloseWidget()
    );
  }

  Container cartCloseWidget() {
    return Container(
      child: Row(
        children: [
          Expanded(
            child: Container(
              height: 0,
            ),
          ),
          Container(
            padding: EdgeInsets.all(4),
            color: Colors.black54,
            child: IconButton(
                icon: Icon(
                  Icons.shopping_cart,
                  color: Colors.white,
                ),
                onPressed: () {

                  // TODO buka cart
                  controller.switchCartWidget();

                }),
          )
        ],
      ),
    );
  }

  Container cartOpenWidget(BuildContext context) {
    return Container(
      width: double.infinity,
      color: Colors.black54,
      padding: EdgeInsets.symmetric(vertical: 14),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 14),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment:
              MainAxisAlignment.spaceBetween,
              children: [
                Obx(
                    () => Text('Rp ${controller.totalPrice}',
                        style: TextStyle(
                            fontSize: 24,
                            color: Colors.white,
                            fontWeight: FontWeight.bold))
                ),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    IconButton(
                        icon: Icon(Icons.clear_all,
                            color: Colors.white),
                        onPressed: () {

                          // TODO clear cart
                          controller.clearCart();

                        }),
                    IconButton(
                        icon: Icon(Icons.send,
                            color: Colors.white),
                        onPressed: () {

                          // TODO Goto Cart Screen
                          // Get.to(CartScreen()); // Kalau menggunakan GetMaterial
                          Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => CartScreen())
                          );

                        }),
                    IconButton(
                        icon: Icon(Icons.expand_more,
                            color: Colors.white),
                        onPressed: () {

                          // TODO tutup cart
                          controller.switchCartWidget();

                        }),
                  ],
                )
              ],
            ),
          ),
          Container(
              height: 78,
              child: Obx(
                  () => ListView(
                    scrollDirection: Axis.horizontal,
                    children: controller.cartList.map((element) => Container(
                      width: 64,
                      height: 64,
                      color: element.color,
                      margin: EdgeInsets.all(7),
                    )).toList(),
                  )
              ))
        ],
      ),
    );
  }

  Container cartEmptyWidget() => Container();
}
