import 'package:cart_state_management/bloc/cart_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../barang.dart';
import 'cart_screen.dart';

class BelanjaScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Container(),
        title: Text('Swalayan Barang'),
        centerTitle: true,
      ),
      body: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          ListView(
            children: generateBarangList()
                .map((item) => ListTile(
              title: Text(item.nama),
              leading: Container(
                width: 64,
                height: 64,
                color: item.color,
              ),
              subtitle: Text('Rp ${item.harga}'),
              onTap: () {

                // TODO ketika barang di pencet, masuk ke keranjang
                BlocProvider.of<CartBLOC>(context).addBarang(item);
                BlocProvider.of<TotalPriceBLOC>(context).addPrice(item.harga);


              },
            ))
                .toList(),
          ),
          cartWidget(context)
        ],
      ),
    );
  }

  Widget cartWidget(BuildContext context) {
    return AnimatedSwitcher(
        duration: Duration(milliseconds: 750),
        transitionBuilder: (Widget child, Animation<double> animation) {
          return ScaleTransition(child: child, scale: animation);
        },
        child: BlocBuilder<CartBLOC, List<Barang>>(
          builder: (ctx, cart){
            return cart.isEmpty
                ? cartEmptyWidget()
                : cartNotEmptyWidget(context);
          },
        )
    );
  }

  Widget cartNotEmptyWidget(BuildContext context){
    return BlocBuilder<CartWidgetBLOC, bool>(builder: (ctx, state){
      return state
          ? cartOpenWidget(context)
          : cartCloseWidget(context);
    });
  }

  Container cartCloseWidget(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Expanded(
            child: Container(
              height: 0,
            ),
          ),
          Container(
            padding: EdgeInsets.all(4),
            color: Colors.black54,
            child: IconButton(
                icon: Icon(
                  Icons.shopping_cart,
                  color: Colors.white,
                ),
                onPressed: () {

                  // TODO buka cart
                  BlocProvider.of<CartWidgetBLOC>(context).switchValue();

                }),
          )
        ],
      ),
    );
  }

  Container cartOpenWidget(BuildContext context) {
    return Container(
      width: double.infinity,
      color: Colors.black54,
      padding: EdgeInsets.symmetric(vertical: 14),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 14),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment:
              MainAxisAlignment.spaceBetween,
              children: [
                BlocBuilder<TotalPriceBLOC, int>(builder: (ctx, totalPrice){
                  return Text('Rp $totalPrice',
                      style: TextStyle(
                          fontSize: 24,
                          color: Colors.white,
                          fontWeight: FontWeight.bold));
                }),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    IconButton(
                        icon: Icon(Icons.clear_all,
                            color: Colors.white),
                        onPressed: () {

                          // TODO clear cart
                          BlocProvider.of<CartBLOC>(context).reset();
                          BlocProvider.of<TotalPriceBLOC>(context).reset();

                        }),
                    IconButton(
                        icon: Icon(Icons.send,
                            color: Colors.white),
                        onPressed: () {

                          // TODO Goto Cart Screen
                          Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => CartScreen())
                          );

                        }),
                    IconButton(
                        icon: Icon(Icons.expand_more,
                            color: Colors.white),
                        onPressed: () {

                          // TODO tutup cart
                          BlocProvider.of<CartWidgetBLOC>(context).switchValue();

                        }),
                  ],
                )
              ],
            ),
          ),
          Container(
              height: 78,
              child: BlocBuilder<CartBLOC, List<Barang>>(
                builder: (ctx, cart){
                  return ListView(
                    scrollDirection: Axis.horizontal,
                    children: cart.map((element) => Container(
                      width: 64,
                      height: 64,
                      color: element.color,
                      margin: EdgeInsets.all(7),
                    )).toList(),
                  );
                },
              ))
        ],
      ),
    );
  }

  Container cartEmptyWidget() => Container();
}
