import 'package:cart_state_management/bloc/cart_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../barang.dart';


class CartScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Column(
            children: [
              SizedBox(
                height: 32,
              ),
              BlocBuilder<TotalPriceBLOC, int>(builder: (ctx, totalPrice){
                return Text('Rp $totalPrice',
                    style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold));
              }),
              SizedBox(
                height: 32,
              ),
              Flexible(
                  child: BlocBuilder<CartBLOC, List<Barang>>(
                    builder: (ctx, cart){
                      return ListView(
                        children: cart.map((element) => ListTile(
                          title: Text(element.nama),
                          leading: Container(
                            width: 64,
                            height: 64,
                            color: element.color,
                          ),
                        )).toList(),
                      );
                    },
                  ))
            ],
          )),
    );
  }
}
