import 'package:cart_state_management/barang.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CartBLOC extends Cubit<List<Barang>>{
  CartBLOC() : super([]);

  void addBarang(Barang item){
    var newState = List<Barang>.from(state);
    newState.add(item);
    emit(newState);
  }

  void reset(){
    emit([]);
  }
}

class TotalPriceBLOC extends Cubit<int>{
  TotalPriceBLOC() : super(0);

  void addPrice(int price){
    emit(state + price);
  }

  void reset(){
    emit(0);
  }
}

class CartWidgetBLOC extends Cubit<bool>{
  CartWidgetBLOC(): super(false);

  void switchValue(){
    emit(!state);
  }
}