import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../barang.dart';


class CartScreen extends StatelessWidget{
  final List<Barang> cartList;
  final int totalPrice;

  CartScreen({this.cartList, this.totalPrice});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Column(
            children: [
              SizedBox(
                height: 32,
              ),
              Text('Rp $totalPrice',
                  style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold)),
              SizedBox(
                height: 32,
              ),
              Flexible(
                  child: ListView(
                    children: cartList.map((element) => ListTile(
                      title: Text(element.nama),
                      leading: Container(
                        width: 64,
                        height: 64,
                        color: element.color,
                      ),
                    )).toList(),
                  ))
            ],
          )),
    );
  }
}
