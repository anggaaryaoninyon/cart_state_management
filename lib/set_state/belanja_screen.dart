import 'package:cart_state_management/set_state/cart_screen.dart';
import 'package:flutter/material.dart';

import '../barang.dart';

class BelanjaScreen extends StatefulWidget {
  @override
  _BelanjaScreenState createState() => _BelanjaScreenState();
}

class _BelanjaScreenState extends State<BelanjaScreen> {
  List<Barang> cartList = [];     // Untuk Menyimpan Barang di cart
  bool isCartOpened = false;      // Untuk mengecek apakah widget cart sedang dibuka atau tidak
  int totalPrice = 0;             // Untuk menyimpan total harga
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Container(),
        title: Text('Swalayan Barang'),
        centerTitle: true,
      ),
      body: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          barangDaganganWidget(),
          cartWidget()
        ],
      ),
    );
  }

  ListView barangDaganganWidget() {
    return ListView(
          children: generateBarangList()
              .map((item) => ListTile(
            title: Text(item.nama),
            leading: Container(
              width: 64,
              height: 64,
              color: item.color,
            ),
            subtitle: Text('Rp ${item.harga}'),
            onTap: () {
      
              // TODO ketika barang di pencet, masuk ke keranjang
              onSelectedBarangClicked(item);
            
            },
          )).toList(),
        );
  }

  void onSelectedBarangClicked(Barang item) {
    setState(() {
      cartList.add(item);
      totalPrice += item.harga;
    });
  }

  Widget cartWidget() {
    return AnimatedSwitcher(
        duration: Duration(milliseconds: 750),
        transitionBuilder: (Widget child, Animation<double> animation) {
          return ScaleTransition(child: child, scale: animation);
        },
        child: cartList.isEmpty       
            ? cartEmptyWidget()
            : cartNotEmptyWidget()
    );
  }

  Widget cartNotEmptyWidget(){
    return isCartOpened
        ? cartOpenWidget()
        : cartCloseWidget();
  }

  Container cartCloseWidget() {
    return Container(
      child: Row(
        children: [
          Expanded(
            child: Container(
              height: 0,
            ),
          ),
          Container(
            padding: EdgeInsets.all(4),
            color: Colors.black54,
            child: IconButton(
                icon: Icon(
                  Icons.shopping_cart,
                  color: Colors.white,
                ),
                onPressed: () {
                  
                  // TODO buka cart
                  setState(() {
                    isCartOpened = true;
                  });
                  
                }),
          )
        ],
      ),
    );
  }

  Container cartOpenWidget() {
    return Container(
      width: double.infinity,
      color: Colors.black54,
      padding: EdgeInsets.symmetric(vertical: 14),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 14),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment:
              MainAxisAlignment.spaceBetween,
              children: [
                Text('Rp $totalPrice',
                    style: TextStyle(
                        fontSize: 24,
                        color: Colors.white,
                        fontWeight: FontWeight.bold)),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    IconButton(
                        icon: Icon(Icons.clear_all,
                            color: Colors.white),
                        onPressed: () {
                          
                          // TODO clear cart
                          setState(() {
                            cartList.clear();
                            totalPrice = 0;
                          });
                          
                        }),
                    IconButton(
                        icon: Icon(Icons.send,
                            color: Colors.white),
                        onPressed: () {
                          
                          // TODO Goto Cart Screen
                          Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => CartScreen(cartList: cartList, totalPrice: totalPrice,))
                          );
                          
                        }),
                    IconButton(
                        icon: Icon(Icons.expand_more,
                            color: Colors.white),
                        onPressed: () {

                          // TODO tutup cart
                          setState(() {
                            isCartOpened = false;
                          });

                        }),
                  ],
                )
              ],
            ),
          ),
          Container(
              height: 78,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: cartList
                    .map((element) => Container(
                  width: 64,
                  height: 64,
                  color: element.color,
                  margin: EdgeInsets.all(7),
                )).toList(),
              ))
        ],
      ),
    );
  }

  Container cartEmptyWidget() => Container();
}
