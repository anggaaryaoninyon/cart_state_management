import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'cart_provider.dart';


class CartScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    var cartProvider = Provider.of<CartProvider>(context, listen: false);

    return Scaffold(
      body: SafeArea(
          child: Column(
            children: [
              SizedBox(
                height: 32,
              ),
              Text('Rp ${cartProvider.totalPrice}',
                  style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold)),
              SizedBox(
                height: 32,
              ),
              Flexible(
                  child: ListView(
                    children: cartProvider.cartList.map((element) => ListTile(
                      title: Text(element.nama),
                      leading: Container(
                        width: 64,
                        height: 64,
                        color: element.color,
                      ),
                    )).toList(),
                  ))
            ],
          )),
    );
  }
}
