import 'package:flutter/cupertino.dart';

import '../barang.dart';

class CartProvider extends ChangeNotifier {
  List<Barang> cartList = [];     // Untuk Menyimpan Barang di cart
  bool isCartOpened = false;      // Untuk mengecek apakah widget cart sedang dibuka atau tidak
  int totalPrice = 0;             // Untuk menyimpan total harga

  void addBarang(Barang item){
    cartList.add(item);
    totalPrice += item.harga;
    notifyListeners();
  }

  void clearCart(){
    cartList.clear();
    totalPrice = 0;
    notifyListeners();
  }

  switchCartWidget(){
    isCartOpened = !isCartOpened;
    notifyListeners();
  }



}