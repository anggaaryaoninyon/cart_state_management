import 'package:cart_state_management/provider/cart_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../barang.dart';
import 'cart_screen.dart';

class BelanjaScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Container(),
        title: Text('Swalayan Barang'),
        centerTitle: true,
      ),
      body: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          ListView(
            children: generateBarangList()
                .map((item) => ListTile(
              title: Text(item.nama),
              leading: Container(
                width: 64,
                height: 64,
                color: item.color,
              ),
              subtitle: Text('Rp ${item.harga}'),
              onTap: () {

                // TODO ketika barang di pencet, masuk ke keranjang
                onSelectedBarangClicked(item, context);

              },
            ))
                .toList(),
          ),
          cartWidget(context)
        ],
      ),
    );
  }

  void onSelectedBarangClicked(Barang item, BuildContext context) {
    var cartProvider = Provider.of<CartProvider>(context, listen: false);
    cartProvider.addBarang(item);
  }

  Widget cartWidget(BuildContext context) {
    return AnimatedSwitcher(
        duration: Duration(milliseconds: 750),
        transitionBuilder: (Widget child, Animation<double> animation) {
          return ScaleTransition(child: child, scale: animation);
        },
        child: Consumer<CartProvider>(
          builder: (ctx, prov, _){
            return prov.cartList.isEmpty
                ? cartEmptyWidget()
                : cartNotEmptyWidget(context);
          },
        )
    );
  }

  Widget cartNotEmptyWidget(BuildContext context){
    var cartProvider = Provider.of<CartProvider>(context);
    return cartProvider.isCartOpened
        ? cartOpenWidget(context)
        : cartCloseWidget();
  }

  Container cartCloseWidget() {
    return Container(
      child: Row(
        children: [
          Expanded(
            child: Container(
              height: 0,
            ),
          ),
          Container(
            padding: EdgeInsets.all(4),
            color: Colors.black54,
            child: Consumer<CartProvider>(
              builder: (ctx, prov, _){
                return IconButton(
                    icon: Icon(
                      Icons.shopping_cart,
                      color: Colors.white,
                    ),
                    onPressed: () {

                      // TODO buka cart
                      prov.switchCartWidget();

                    });
              },
            ),
          )
        ],
      ),
    );
  }

  Container cartOpenWidget(BuildContext context) {
    return Container(
      width: double.infinity,
      color: Colors.black54,
      padding: EdgeInsets.symmetric(vertical: 14),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 14),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment:
              MainAxisAlignment.spaceBetween,
              children: [
                Consumer<CartProvider>(
                  builder: (ctx, prov, _){
                    return Text('Rp ${prov.totalPrice}',
                        style: TextStyle(
                            fontSize: 24,
                            color: Colors.white,
                            fontWeight: FontWeight.bold));
                  },
                ),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Consumer<CartProvider>(
                      builder: (ctx, prov, _){
                        return IconButton(
                            icon: Icon(Icons.clear_all,
                                color: Colors.white),
                            onPressed: () {

                              // TODO clear cart
                              prov.clearCart();

                            });
                      },
                    ),
                    IconButton(
                        icon: Icon(Icons.send,
                            color: Colors.white),
                        onPressed: () {

                          // TODO Goto Cart Screen
                          Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => CartScreen())
                          );

                        }),
                    Consumer<CartProvider>(
                      builder: (ctx, prov, _){
                        return IconButton(
                            icon: Icon(Icons.expand_more,
                                color: Colors.white),
                            onPressed: () {

                              // TODO tutup cart
                              prov.switchCartWidget();

                            });
                      },
                    ),
                  ],
                )
              ],
            ),
          ),
          Container(
              height: 78,
              child: Consumer<CartProvider>(
                builder: (ctx, prov, _){
                  return ListView(
                    scrollDirection: Axis.horizontal,
                    children: prov.cartList.map((element) => Container(
                      width: 64,
                      height: 64,
                      color: element.color,
                      margin: EdgeInsets.all(7),
                    )).toList(),
                  );
                },
              ))
        ],
      ),
    );
  }

  Container cartEmptyWidget() => Container();
}
