import 'package:cart_state_management/bloc/cart_bloc.dart';
import 'package:cart_state_management/getx/cart_controller.dart';
import 'package:cart_state_management/provider/cart_provider.dart';
import 'package:cart_state_management/set_state/belanja_screen.dart'
    as setState;
import 'package:cart_state_management/provider/belanja_screen.dart' as provider;
import 'package:cart_state_management/bloc/belanja_screen.dart' as bloc;
import 'package:cart_state_management/getx/belanja_screen.dart' as getx;
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(ChangeNotifierProvider(
    create: (context) => CartProvider(),
    child: MultiBlocProvider(
      providers: [
        BlocProvider(create: (ctx) => CartBLOC()),
        BlocProvider(create: (ctx) => TotalPriceBLOC()),
        BlocProvider(create: (ctx) => CartWidgetBLOC())
      ],
      child: MyApp(),
    ),
  ));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: [
          ListTile(
            title: Text('Set State'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => setState.BelanjaScreen()));
            },
          ),
          ListTile(
            title: Text('Provider'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => provider.BelanjaScreen()));
            },
          ),
          ListTile(
            title: Text('BLOC'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => bloc.BelanjaScreen()));
            },
          ),
          ListTile(
            title: Text('GetX'),
            onTap: () {
              Get.put(CartController());
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => getx.BelanjaScreen()));
            },
          ),
        ],
      ),
    );
  }
}
